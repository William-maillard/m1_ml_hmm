# DSC1 Machine learning

This repository containes jupyter notebook made during practical sessions and project (sentiment analysis) of the course Machine Learning from my M1.  
If you want to open this repository into a python executable environement (binder) please follow the link bellow :  
[https://mybinder.org/v2/gl/William-maillard%2Fm1_ml_hmm/main?labpath=README.md](https://mybinder.org/v2/gl/William-maillard%2Fm1_ml_hmm/main?labpath=README.md)

The course was about :

- training ML algorithms
- k-Nearest Neighbors
- Decision Tree and Random Forest
- Support Vectors Machine
- Hidden Markov Model

## Notebook summary

Comming soon ... 

### TP1_ML_Sklearn

### TP2_ML_DT

### TP3_ML_SVM

### TP4_Projet_HMM_sentiment_analysis
